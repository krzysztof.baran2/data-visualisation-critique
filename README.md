# Data Visualisation Critique

* In this essay, I critique the effectiveness of three visualisations and recommend improvements to one of them.
* This essay was submitted for an assessment in my module Data Processing and Visualisation.
* References and citations are included.
* I believe this critique is evidence of my vast knowledge of data visualisation best practices.
* The critique can be viewed [here](Data Visualisations Critique.pdf).
